#There will be a lot of modifications, because of this is a laboratory work for UTMN students
#for example: key().p is an odd value, not prime; encrypt().r is a range(0, 2**rho2) and so on ...

import random

eta = 8         #p (key)
gamma = 32      #q
tau = 5         #Xi
rho1 = 1        #key noise
rho2 = 2        #cipher noise

def key():
    while 1 == 1:
        p = random.randint(2**(eta-1), 2**eta)
        if p % 2 == 1:
            break

    while 1 == 1:
        D = [0 * i for i in range(tau+1)]
        q = [random.randint(0, (2**gamma // p) -1) for i in range(tau+1)]
        r = [random.randint(-2**rho1+1, 2**rho1-1) for i in range(tau+1)]
        for i in range(tau+1):
           D[i] = p * q[i] + r[i]
        sorted(D, reverse=True)
        if D[0] % 2 == 1 and (D[0] % p) % 2 == 0:
            break

    #x0 = [i for i in sorted(D, reverse=True) if i % 2 == 1 and (i % p) % 2 == 0][0]  #sorted array, where max element accept rules
    pk = D
    sk = p
    return pk, sk
#print key()

def enc(pk, m):
    left = random.randint(1, tau)
    right = random.randint(1, tau)
    if right == left:
        s = []
        s.append(right)
    else:
        s = range(min(sorted([left, right])), max(sorted([left, right])))

    
    index = [random.choice(range(1, tau)) for i in range(random.randint(2, tau))]
    r = random.randint(0, 2**rho2)
    buf = [0 * i for i in range(tau+1)]
    for i in index:                 #change index <=> s
        buf[i] = pk[i]
    c = m + 2*r + 2*long(sum(buf)) % pk[0]
    return c
#print enc(key()[0], 1)

def decrypt(c, sk):
    m = (c % sk) % 2
    return m
print decrypt(enc(key()[0], 1), key()[1])
