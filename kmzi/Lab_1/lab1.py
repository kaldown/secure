import sys
from copy import deepcopy

class Digits:
    def __init__(self, A, B):
        self.C = [0] * 128
        self.Ab = A
        self.Bb = B
        A = [1 if digits == '1' else 0 for digits in bin(A)[2:]]
        B = [1 if digits == '1' else 0 for digits in bin(B)[2:]]
        if len(A) > 128:
            sys.exit('A > 128 bit')
        elif len(B) > 128:
            sys.exit('B > 128 bit')
        else:
            A.reverse()
            B.reverse()
            for i in range(128 - len(A)):
                A.append(0)
            for i in range(128 - len(B)):
                B.append(0)
            self.A = A
            self.A0 = A[:64]
            self.A1 = A[64:128]
            self.B = B
            self.B0 = B[:64]
            self.B1 = B[64:128]
            self.C0 = self.C[:64]
            self.C1 = self.C[64:128]
                    
    def Print(self):
        print "A0 =", self.A0 
        print "A1 =", self.A1 
        print "B0 =", self.B0 
        print "B1 =", self.B1

    def arrsum(self, s=False):
        C = [0] * 128
        for i in range(128):
            if self.A[i] + self.B[i] + s >= 2:
                C[i] = self.A[i] ^ self.B[i] ^ s
                s = True
            else: 
                C[i] = self.A[i] ^ self.B[i] ^ s
                s = False
        if s == True: C.append(1)
        c = deepcopy(C)
        self.C = C
        c.reverse()
        return c

    def arrdif(self, s=False):
        if self.Ab < self.Bb: 
            sys.exit('A < B')
        else:
            Bc = ~self.Bb & 0xFFFFFFFF
            buf = [1 if digits == '1' else 0 for digits in bin(Bc)[3:]]
            buf.reverse()
            for i in range(128 - len(buf)):
                buf.append(0)

            Cbuff = [0] * 128
            for i in range(128):
                if self.A[i] + buf[i] + s >= 2:
                    Cbuff[i] = self.A[i] ^ buf[i] ^ s
                    s = True
                else: 
                    Cbuff[i] = self.A[i] ^ buf[i] ^ s
                    s = False
            if s == True: Cbuff.append(1)

            C = [0] * 128
            Empty = [1]
            for i in range(127):
                Empty.append(0)
            for i in range(128):
                if Cbuff[i] + Empty[i] + s >= 2:
                    C[i] = Cbuff[i] ^ Empty[i] ^ s
                    s = True
                else: 
                    C[i] = Cbuff[i] ^ Empty[i] ^ s
                    s = False
            if s == True: C.append(1)

            c = deepcopy(C)
            c.reverse()
            for i in range(len(c)):
                if c[i] == 1:
                    c[i] = 0
                    break
            Cb = deepcopy(c)
            Cb.reverse()
            self.C = Cb
            return c

    def toBit(a):
        A = [1 if digits == '1' else 0 for digits in bin(a)[2:]]
        return A

    def toInt(a):
        A = int("".join(str(x) for x in a), 2)
        return A
