# -*- coding: utf-8 -*-
from functools import partial
from random import shuffle

cards_samples = range(2,15)
diamonds = 100
hearts = 200
clubs = 300
spades = 400
suits = ['diamonds', 'hearts', 'clubs', 'spades']
cards = [(x[0]+1)*100 for x in enumerate(suits)]
cards = [d+k for k in cards_samples for d in cards]

#inputed = input_data("number of players")
#assert isinstance(inputed, int)
####
players_number = 3
        
p = 599
#####

players = dict.fromkeys(xrange(players_number), 
                        dict.fromkeys(["C", 
                                       "D", 
                                       "cards"]
                                    ))

players[0] = {"C": 177,
              "D": 473,
              "cards": [],
             }

players[1] = {"C": 457,
              "D": 475,
              "cards": [],
             }
             
players[2] = {"C": 387,
              "D": 17,
              "cards": [],
             }


def encrypt_card(x, player):
    global p
    return pow(x, player["C"], p)

def decrypt_card(x, player):
    global p
    return pow(x, player["D"], p)

def encrypted_cards():
    global cards
    global players
    #shuffle(cards)
    for player_id in range(players_number):
        player = players[player_id]
        cards = map(partial(encrypt_card, player=player), cards)
        print "Encrypted cards #{0} => {1}\t{2}".format(player_id, player_id+1, cards)

def pic_card(p_card):
    pic_card = p_card % 100
    if pic_card == 11:
        ult = "J"
    if pic_card == 12:
        ult = "Q"
    if pic_card == 13:
        ult = "K"
    if pic_card == 14:
        ult = "A"
    else:
        return str(pic_card)
    return ult

def determine_card(p_card):
    suit_card = p_card / 100
    if suit_card == 1:
        res = "Diamonds"
        ult = pic_card(p_card)
    elif suit_card == 2:
        res = "Hearts"
        ult = pic_card(p_card)
    elif suit_card == 3:
        res = "Clubs"
        ult = pic_card(p_card)
    elif suit_card == 4:
        res = "Spades"
        ult = pic_card(p_card)
    else:
        print "WRONG SUIT"
        exit(0)
    result = res + ' ' + ult
    return result

# DON'T USE
def decrypted_cards(till_player_id=players_number):
    temp_cards = cards
    global players
    shuffle(cards)
    for player_id in range(players_number)[:till_player_id:-1]:
        player = players[player_id]
        temp_cards = map(partial(decrypt_card, player=player), temp_cards)
        print "Decrypted cards#{0} => {1}\t{2}".format(player_id, player_id-1, temp_cards)
    return temp_cards

# actually decrypts whole set, JK! 
def decrypt_one_card_fix(card_number, player_id):
    global cards
    global players
    temp_card = card_number
    try:
        assert card_number in cards
    except:
        print "There is no such {0} in {1}".format(card_number, cards)
        exit(0)
    for p_id in range(players_number)[player_id+1:]:
        player = players[p_id]
        temp_card = decrypt_card(temp_card, player=player)
        print "Decrypted one card #{0} => {1}\t{2}".format(p_id, p_id+1, temp_card)
        print player['D']
    for p_id in range(players_number)[:player_id+1]:
        player = players[p_id]
        temp_card = decrypt_card(temp_card, player=player)
        print "Decrypted one card #{0} => {1}\t{2}".format(p_id, p_id+1, temp_card)
    temp_card = determine_card(temp_card)
    return temp_card
        
# DON'T USE
def decrypt_one_card(card_number, player_id):
    temp_cards = cards
    global players
    try:
        assert card_number in cards
    except:
        print "There is no such {0} in {1}".format(card_number, cards)
        exit(0)
    for player_id in range(players_number)[player_id:]:
        player = players[player_id]
        temp_cards = map(partial(decrypt_card, player=player), temp_cards)
        print "Decrypted one card #{0} => {1}\t{2}".format(player_id, player_id-1, temp_cards)
    for player_id in range(players_number)[:player_id]:
        player = players[player_id]
        temp_cards = map(partial(decrypt_card, player=player), temp_cards)
        print "Decrypted one card #{0} => {1}\t{2}".format(player_id, player_id-1, temp_cards)
    return temp_cards

encrypted_cards()



def take_card(card_number, player_id=None):
    global cards
    global players
    player = players[player_id]
    try:
        assert player_id != None
        assert card_number in cards
    except:
        print "player_id should't be None or there is no such card number"
        exit(0)
    #temp_cards = decrypt_one_card(5, 3)
    #print "TEMP CARDS %s" % temp_cards
    player["cards"].append(decrypt_one_card_fix(card_number, player_id))
    cards.remove(card_number)


def write_results():
    for player in players.iteritems():
        p_cards = player[1]['cards']
        with open("{0}.txt".format(player[0]), 'w+') as f:
            f.write("Player #{0} have cards: {1}\n"
                    .format(player[0], player[1]['cards']))

take_card(57, player_id=0)
print players
write_results()

