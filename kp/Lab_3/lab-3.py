import md5

def hash_func(Y, b, R1, R2):
    y = Y ^ (R1 << 33)
    y = Y ^ (R2 << 1)
    y = y ^ b
    h = md5.new()
    h.update(str(y))
    return h.hexdigest()

Y = 0xE67CFB87AB27C82DC3483356F736CA32
R1 = 1739039209
R2 = 1494684564
b = 0

print hash_func(Y, b, R1, R2)
