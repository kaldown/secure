from pygost import gost28147_89
from fractions import gcd

kp = range(30)
kp = ''.join([str(x) for x in kp])[:32]

K1 = kp
K2 = kp

a = gost28147_89.MAC(K1) # instance of ch digest for A
b = gost28147_89.MAC(K1) # same for B

Alice = {'K1': K1,
         'K2': K2,
        }

Bob = {'K1': K1,
       'K2': K2,
      }

r1 = 123456
r1 = str(r1)

a.update(r1)
alice_text = a.digest().encode('hex')

b.update(r1)
bob_text = b.digest().encode('hex')

assert alice_text == bob_text
print 'Verified'

