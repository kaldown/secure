from pygost import gost28147_89

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

# ====KEY GEN ===
p = 2347
q = 23
g = 266
x = 1468

y = pow(g, x, p)
# ==============

#==== Signing ===
m = 1337
h = m % 107
k = 19
k1 = modinv(k, q)
r = pow(g, k, p) % q
s = k1 * (h + x*r) % q

signature = m, r, s
#==============

#==== Verif ===
h = m % 107
w = modinv(s, q)
u1 = h*w % q
u2 = r*w % q
v = (g**u1 * y**u2 % p) % q
assert v == r
print 'Verified\n'
#==============

#===== extract msg ===
m_verif = w * (h + x*v) % q
assert m_verif == k
print m_verif
print 'Message eq'
#=====================

