from fractions import gcd

def calculate_subgroups(modulus):
    def get_group(coprime):
        return tuple(sorted(generate_subgroup(coprime, modulus)))

    return {get_group(coprime) for coprime in get_coprimes(modulus)}

def generate_subgroup(factor, modulus, start=1):
    while True:
        yield start
        start = (start * factor) % modulus

        if start == 1:
            return

def get_coprimes(number):
    return (i for i in range(1, number) if gcd(number, i) == 1)

grp =  calculate_subgroups(42)
grp = sorted({x for k in grp for x in k})
print grp
