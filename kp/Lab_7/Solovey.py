import random as rnd
import methods

def Solovey(n, t=5):
	for i in range(t):
		a = aGen(n)
		if methods.euclid(a, n) != 1: return False
		r = methods.jacobi(a, n)
		if r != methods.sravn(a, n): return False
	return True

def aGen(n):
	return rnd.randint(2, n-2)
