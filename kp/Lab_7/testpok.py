import random as rnd

def testpocklington(n, t, q):
    a = ajgen(t, n)
    if ajTest(a, n) == False: return False
    else:
        if qjTest(a, n, q) == True: return True
        else: return 0

def ajgen(t, n):
    a = [0] * t
    for j in range(t):
        a[j] = rnd.randint(2, n-1)
    return a

def ajTest(a, n):
    if 1 not in map(lambda x: pow(x, n-1, n), a): return False
    else: return True

def qjTest(a, n, q):
    for ai in a:
        if 1 in filter(lambda x: pow(ai, (n-1)/x, n) == 1, q):
            if ai == a[-1]: return False
            else: continue
        else: break
    return True
