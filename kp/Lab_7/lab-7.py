from pokGen import pok10
from Solovey import Solovey
from random import randint, choice
from fractions import gcd

#================
#q = 443
#p = 48731
#g = 11444 # a
#w = 357   # s, private key
##y = pow(modinv(g, p), w, p) # Alice's open key, same as pow(g, -w, p) - eX Euclid algo
##key = (p,q,g,y)
#
#r = 274   # Alice's random val
#x = pow(g, r, p) # Alice -> Bob
#
#e = 129   # Bob's random val
#s = r + w * e % q # Alice -> Bob
#
##z = g ** s * y ** e % p

#=====PARAMS=====
t = 4
#================

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

def authentication():
    e = random.range(1, 2**t)
    y = pow(modinv(g, p), w, p)
    if x == (a**y * v**e) % p:
        return True
    else:
        return False

#==========

def gen_q(power):
    global t
    while True:
        nMASS, q, pok, sol, Buff = pok10(power, 1, 1)
        prime_to_test = nMASS[0]
        if Solovey(prime_to_test) == True and q > pow(2, t):
            return prime_to_test
        else:
            continue

def gen_p(_q):
    _r = 1
    while True:
        p = _q * 2 * _r + 1
        try:
            if Solovey(p) == True:
                return p
            else:
                _r += 1
        except:
            pass
        
def gen_e():
    global t
    return randint(1, 2 ** t)

def gen_a(p,q,g):
    return pow(g, (p-1)/q, p)

def gen_v(a, s):
    return pow(modinv(a, p), s, p)

def gen_x(a, r, p):
    return pow(a, r, p)

def gen_y(r, s, e, q):
    return pow(r+s*e, 1, q)

def calculate_subgroups(modulus):
    def get_group(coprime):
        return tuple(sorted(generate_subgroup(coprime, modulus)))

    return {get_group(coprime) for coprime in get_coprimes(modulus)}

def generate_subgroup(factor, modulus, start=1):
    while True:
        yield start
        start = (start * factor) % modulus

        if start == 1:
            return

def get_coprimes(number):
    return (i for i in range(1, number) if gcd(number, i) == 1)




def gen_g(p):
    prime = p
    num_to_check = 0
    p_minus_1_range = range(1,prime)
    primitive_roots = []
    for each in range(1, prime):
            num_to_check += 1
            candidate_prim_roots = []
            for i in range(1, prime):
                    modulus = (num_to_check ** i) % prime
                    candidate_prim_roots.append(modulus)
                    cleanedup_candidate_prim_roots = set(candidate_prim_roots)
                    if len(cleanedup_candidate_prim_roots) == len(range(1,prime)):
                            primitive_roots.append(num_to_check)
    return primitive_roots




#q = gen_q(10)
#print q
#p = gen_p(q)
#print p
#print gen_g(1559)
p = 1559
q = 41
g = 19
a = 1490
s = 7
r = 9
e = 6
x = 240
y = 10
v = 546
print x == (a**y * v**e) % p

#grp =  calculate_subgroups(14173)
#grp = sorted({x for k in grp for x in k})
#print grp
