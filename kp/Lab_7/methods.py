import random as rnd
import math

def sravn(a, n):
	if pow(a, int((n - 1) / 2), n) == 1: return 1
	elif pow(a, int((n - 1) / 2), -1*n) == -1: return -1
	elif pow(a, int((n - 1) / 2), n) == 0: return 0


def gen():
	while True:
		I = rnd.randint(2**127, 2**128-4)
		if I % 2 == 1:
			break
	return I

def toBit(a):
	A = [1 if digits == '1' else 0 for digits in bin(a)[2:]]
	return A
def toInt(a):
	A = int("".join(str(x) for x in a), 2)
	return A
	
def jacobi(a, n):
    s = 1
    while True:
        if n < 1: raise ValueError("Too small  " + str(n))
        if n & 1 == 0: raise ValueError("Jacobi only for hetnoe")
        if n == 1: return s
        a = a % n
        if a == 0: return 0
        if a == 1: return s

        if a & 1 == 0:
            if n % 8 in (3, 5):
                s = -s
            a >>= 1
            continue

        if a % 4 == 3 and n % 4 == 3:
            s = -s

        a, n = n, a
    return

"""
def jacobi(n, m):
	r = 1
	if(euclid(n, m) != 1): 
		r = 0
	else:
		while True:
			n = n % m
			k, n = razlojenie(n, 2)[0], razlojenie(n, 2)[1]
			if(k == 1):
				if(m % 8) in (3, 5): r = -r
			if(n == 1): break
			if(n == m-1 and m % 4 == 1): break
			if(n == m-1 and m % 4 == 3):
				r = -r
				break
			n, m = m, n
			r = r*(-1)**(((m-1)/2)*((n-1)/2))
	return r
"""

def euclid(a, b):
	while a != 0 and b != 0:
		if a > b:
			a = a % b
		else:
			b = b % a
	return a + b

def razlojenie(a, p):
    """
    Return s, t such that  a = p**s * t,  t % p = 0
    """
    s = 0
    if p > 2:
        while a and a % p == 0:
            s += 1
            a //= p
    elif p == 2:
        while a and a & 1 == 0:
            s += 1
            a >>= 1
    else:
        raise ValueError("Number %d is not a prime (is smaller than 2)" % p)
    return s, a

def pi(x):
	A = 2**x / math.log(2**x)
	return float(A)

def etta(x):
	A = pi(x) - pi(x-1)
	return float(A)

def assympt(a):
	A = float(etta(a) / 2**(a-2))
	B = 1/A
	return ('%.3f') % B

#print(assympt(128))