import eratosphen as primes
import random as rnd
import testpok
import eratosphen
import Solovey
import methods

def FGen(l):
	k = l / 2 + 1 #count of qi and it's pow (alpha)
	#k = int(rnd.random() * (10 ** (10 * rnd.random())))
	while True:
		erat = primes.eratosphen(500)
		F = 1
		Range = rnd.randint(1, k)
		q = [0] * Range
		for i in range(Range):
			qBuff = rnd.choice(erat)
			erat.remove(qBuff)
			q[i] = pow(qBuff, rnd.choice(range(k)))
			F *= q[i]
			if len(bin(F)[2:]) == ((l / 2) + 1): 
				eraser(q)
				return F, q
			elif len(bin(F)[2:]) >= ((l / 2) + 1): break

def RGen(F):		
	lenR = len(bin(F)[2:]) - 1
	while True:
		R = rnd.randint(2**(lenR-1), 2**lenR-1)
		if len(bin(R)[2:]) == lenR and R % 2 == 0: return R

def nGen(l):
	F, q = FGen(l)
	R = RGen(F)
	n = R * F + 1
	return n, q

def PockTrue(l, t):
	NegPokCount = 0
	NegSolCount = 0
	NegPokBuff = []
	while True:
		n, q = nGen(l)
		if testpok.testpocklington(n, t, q) == True: return n, q, NegPokCount, NegSolCount, NegPokBuff
		else: 
			NegPokBuff.append(n)
			NegPokCount += 1
			if Solovey.Solovey(n) == True: NegSolCount += 1

def pok10(l, t, cTimes):
	n = [0] * cTimes
	q = [0] * cTimes
	NegPokCount = [0] * cTimes
	NegSolCount = [0] * cTimes
	for i in range(cTimes):
		n[i], q[i], NegPokCount[i], NegSolCount[i], NegPokBuff = PockTrue(l, t)
	eraser(NegPokCount)
	eraser(NegSolCount)
	return n, q, NegPokCount, NegSolCount, NegPokBuff

def eraser(q):
	while 0 in q: q.remove(0)
	return q

