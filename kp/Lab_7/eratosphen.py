import math

def eratosphen(n):
	nums = range(2, n)
	for i in range(2, int(math.ceil(math.sqrt(n)))):
		nums = filter(lambda x: x == i or x % i, nums)
	return nums
