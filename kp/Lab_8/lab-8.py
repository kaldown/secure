import fractions
import md5
import operator
import functools
import random

def phi(n):
    amount = 0

    for k in range(1, n + 1):
        if fractions.gcd(n, k) == 1:
            amount += 1

    return amount

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

def F(num):
    return num + 1

def get_m(num):
    primes = [3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53]
    iter = bin(num)[2:]
    iter = [int(x) for x in iter]
    iter = reversed(iter)
    check = zip(iter, primes)
    multiply = [x[1] for x in check if x[0] == True]
    res = functools.reduce(operator.mul, multiply, 1)
    return res


rmin = 2
rmax = 10

account = 500
M = 100
m = get_m(M)
p = 19
q = 5
n = p*q


print "A:"
N = random.randint(rmin, rmax)
r = random.randint(rmin, rmax)
print "N %s" % N
print "r %s\n" % r

print "A -> T:"
#md = md5.new()
#md.update(str(232))
#m = int(md.hexdigest(), 16)
b = F(n) * pow(r, m, n)
print "b %s\n" % b


print "T -> A:"
fi = phi(n)
#print "fi %s" % fi

y = pow(b, pow(modinv(m, fi), 1, fi), n)
print "y %s\n" % y


print "A:"
while True:
    try:
        x = y * pow(modinv(r, n), 1, n)
        break
    except:
        r = random.randint(rmin, rmax)

print "x %s" % x

banknote = N, x
print "banknote {0}\n".format(banknote)



print "Platej:"
print 'Account: %s\n'% (account - M)
print "A:"
s = 40

S = M - s
print "S %s" % S

Sm = get_m(S)
print "d %s" % Sm
print "N* %s\n" % random.randint(rmin, rmax)

print "A -> B:"

x = F(n) * pow(r, S, n)
print "x %s\n" % x

print "B -> T:"
print x, S, Sm, m, '\n'


print 'Return change to the account'
print 'Account after recieved change: %s' % (account - S)
