#-*- coding:utf-8 -*-

import random

__all__ = ('NULL_POINT', 'Curve')

NULL_POINT = (None, None)

def xgcd(a, b):
    if a == 0: return 0, 1, b
    if b == 0: return 1, 0, a

    px, ppx = 0, 1
    py, ppy = 1, 0

    while b:
        q = a // b
        a, b = b, a % b
        x = ppx - q * px
        y = ppy - q * py
        ppx, px = px, x
        ppy, py = py, y

    return ppx, ppy, a

def invmod(a, n):
    if n < 2:
        raise ValueError("modulus must be greater than 1")

    x, y, g = xgcd(a, n)

    if g != 1:
        raise ValueError("no invmod for given @a and @n")
    else:
        return x % n


class Curve:
    def __init__(self, a, b, p, g=None):
        self.a = a
        self.b = b
        self.module = p

        self.g = g
        return None

    def is_null(self, p):
        return p == NULL_POINT

    def is_opposite(self, p1, p2):
        x1, y1 = p1
        x2, y2 = p2
        return (x1 == x2 and y1 == -y2 % self.module)

    def right(self, x):
        return (x ** 3 + self.a * x + self.b) % self.module

    def add(self, p1, p2):
        if self.is_null(p1):
            return p2

        if self.is_null(p2):
            return p1

        if self.is_opposite(p1, p2):
            return NULL_POINT

        x1, y1 = p1
        x2, y2 = p2

        l = 0
        if x1 != x2:
            l = (y2 - y1) * invmod(x2 - x1, self.module)
        else:
            l = (3 * x1 ** 2 + self.a) * invmod(2 * y1, self.module)

        x = (l * l - x1 - x2) % self.module
        y = (l * (x1 - x) - y1) % self.module  # yes, it's that new x
        return (x, y)

    def power(self, p, n):
        if n == 0 or self.is_null(p):
            return NULL_POINT

        res = NULL_POINT
        while n:
            if n & 1:
                res = self.add(res, p)
            p = self.add(p, p)
            n >>= 1
        return res

    def get_order(self, p):
        order = 1
        res = p
        while not self.is_null(res):
            res = self.add(res, p)
            order += 1
        return order
