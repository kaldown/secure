# -*- coding: utf-8 -*-

from ast import literal_eval
import os
from functools import partial
from math import ceil
from Solovey import Solovey as PrimeTest

def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
 
    for n_i, a_i in zip(n, a):
        p = prod / n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod
 
 
def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a / b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1
 
def gen_secrets(m, k, n):
    g_list = list()
    secret_list = list()
    for iter in range(n):
        g = pow(m, 1/float(k))
        g = int(ceil(g))
        while len(g_list) != 5:
            try:
                res = PrimeTest(g)
                if res and g not in g_list: 
                    g_list.append(g)
                    secret_list.append(m % g)
                else: 
                    if g % 2 == 0:
                        g += 1
                    else:
                        g += 2
            except:
                pass
    return zip(secret_list, g_list)

def get_message(secrets, k):
    assert len(secrets) >= k
    a, n = zip(*secrets[:k])
    return chinese_remainder(n, a)

def write_files(secrets):
    if os.path.exists('secret_folder'):
        for iter in range(len(secrets)):
            path = os.path.join('./secret_folder', "{0}-player.txt".format(iter))
            with open(path, 'w+') as f:
                f.write(str(secrets[iter]))
    else:
        os.mkdir('secret_folder')
        write_files(secrets)

def read_files(k):
    if os.path.exists('secret_folder'):
        secrets = list()
        path = sorted(os.listdir('./secret_folder'))
        for j in path:
            file_name = os.path.join('secret_folder', j)
            with open(file_name, 'r+') as f:
                partial_secrets = literal_eval(f.read())
                secrets.append(partial_secrets)

        if k > len(path): raise Exception("parts of secrets < k")
        return get_message(secrets, k)
    else:
        raise Exception("secret_folder doesn't exists")
    
secrets =  gen_secrets(987542, 3, 5)
print "Secrets\n\t{0}".format('\n\t'.join([str(s) for s in secrets]))
print "\nMessage => {0}\n".format(get_message(secrets, 3))

#write_files(secrets)
print read_files(3)
